# Criando pipeline no Jenkins

Agora vamos criar um pipeline no Jenkins para instalar o Wordpress.


## Pré-requisitos

Certifique-se de ter o Jenkins instalado e configurado em seu kubernetes.

[Instale e configure o MySQL no kubernetes](./install_mysql.md).

Crie o volume para o Wordpress. Dentro da máquina virtual ou no minikube execute o comando:

		sudo mkdir -p /data/wordpress

Após criar a pasta, altere o usuário e o grupo da pasta do wordpress:

		sudo chown -R 1000:1000 /data/wordpress

## Configurando o Git

Caso o repositório Git seja **private**, você precisará adicionar uma chave SSH pública nele para que o Jenkins possa acessá-lo. Neste projeto estamos utilizando o [GitLab](https://gitlab.com/). 

Para adicionar a chave, abra o repositório no GitLab e clique em **Settings** e depois em **Repository**. Na página de configuração do repositório, expanda a opção **Deploy keys** e adicione a chave SSH pública.

<img src="assets/Captura de tela de 2022-03-04 10-43-29.png" alt="" width="700"/>


## Criando o pipeline (Free Style)

Abra o Jenkins e vá em **`novo job`**. Crie um job chamado `deploy-wordpress` com a configuração **`freeStyle`**.

Nas configurações gerais do job, marque a opção `Este build é parametrizado`. Com esta opção habilitada poderemos utilizar variáveis no ambiente do Jenkins. Você irá adicionar dois **parâmetros de texto**:

- USER: usuário da sua VM
- HOST: IP da sua VM

Estas variáveis serão utilizadas para fazer a conexão com a máquina virtual.

Agora defina o **Gerenciador de código fonte** como **Git**. Em `Repository URL` coloque o endereço SSH do repositório Git. Neste projeto, o link do repositório é: `git@gitlab.com:JadsonBraz/wordpress.git`.

<img src="assets/Captura de tela de 2022-03-04 17-41-53.png" alt="" width="700"/>


Em `Build Trigger` marque a opção `Consultar periodicamente o SCM`. E dentro da caixa de texto coloque cinco asteríscos, como na imagem abaixo, para que o Jenkins consulte o repositório a cada minuto.

<img src="assets/Captura de tela de 2022-02-28 15-29-09.png" alt="" width="700"/>


Indo agora para o `Ambiente de Build`, marque a opção `Use secret text or file` e coloque a **chave SSH privada** utilizada para acessar sua máquina virtual. Isso é importante porque sem essa chave, o Jenkins não conseguirá acessar a máquina virtual que está rodando o kubernetes. no *input* `key file variable` você dá um nome para a chave.

<img src="assets/Captura de tela de 2022-03-04 17-42-04.png" alt="" width="700"/>


Agora é hora de criar o script que será responsável por fazer o **deploy do Wordpress**. Em `Build`, adicione o passo `Executar shell`. Na caixa de texto coloque os comandos abaixo, modificando as variáveis `USER`, `HOST` e o **`nome da variável que você deu à sua chave SSH`**:

~~~sh
ssh -o StrictHostKeyChecking=no $USER@$HOST -i $awsssh mkdir -p /home/$USER/wordpress

scp -i $awsssh "wordpress-deployment.yaml" $USER@$HOST:/home/$USER/wordpress/wordpress-deployment.yaml

ssh $USER@$HOST -i $awsssh kubectl apply -f /home/$USER/wordpress/wordpress-deployment.yaml
~~~

O código acima executará os seguintes passos:
- Criar um diretório na home do usuário com o nome `wordpress`
- Copiar o arquivo `wordpress-deployment.yaml` para o diretório `wordpress`
- Aplicar o deploy do Wordpress, executando o comando `kubectl apply -f wordpress-deployment.yaml`

Após ter feito isso, clique em **`Salvar`** e execute o job.

## Criando o pipeline (Pipeline)

Outra opção que temos no Jenkins é executar o pipeline como código. Para isso, crie um novo job chamado `deploy-wordpress-pipeline` com a configuração **`pipeline`**.

Nas configurações gerais do job, marque a opção `Este build é parametrizado`. Com esta opção habilitada poderemos utilizar variáveis no ambiente do Jenkins. Você irá adicionar dois **parâmetros de texto**:

- USER: usuário da sua VM
- HOST: IP da sua VM

Estas variáveis serão utilizadas para fazer a conexão com a máquina virtual.

Em `Build Trigger` marque a opção `Consultar periodicamente o SCM`. E dentro da caixa de texto coloque cinco asteríscos, como na imagem abaixo, para que o Jenkins consulte o repositório a cada minuto.

No `Pipeline` você irá definir o script a partir do SCM utilizando a opção `Git`. Coloque o link do repositório e a chave SSH privada que dá acesso a esse repositório. Mude a branch principal, caso necessário. O **Script Path** é o caminho para o script que será executado.

E onde coloco o Script do Pipeline?

Bom, o script do pipeline ficará na pasta raís do seu repositório que contém a aplicação, com o nome `Jenkinsfile`. A aplicação desse projeto pode ser acessada no link [https://gitlab.com/JadsonBraz/wordpress](https://gitlab.com/JadsonBraz/wordpress).

O script tem o seguinte conteúdo:

~~~groovy
pipeline {
    agent any

    stages {

        stage('Send wordpress-deployment') {
            steps {
                sshagent(credentials: ['awsssh']) {
                    sh '''
                        scp "wordpress-deployment.yaml" $USER@$HOST:/tmp/wordpress-deployment.yaml
                    '''
                }
            }
        }

        stage('Apply wordpress-deployment') {
            steps {
                sshagent(credentials: ['awsssh']) {
                    sh '''
                        ssh $USER@$HOST kubectl apply -f /tmp/wordpress-deployment.yaml
                    '''
                }
            }
        }
    }
}
~~~

São basicamente duas etapas:
- Copiar o arquivo `wordpress-deployment.yaml` para a máquina virtual
- Aplicar o deploy do Wordpress

Após ter feito isso, clique em **`Salvar`** e execute o job.


## Verificando o deploy

Para verificar o deploy, edite o seu arquivo hosts e adicione o IP da máquina virtual que está rodando o kubernetes, com o URL `projetofinal.compass.uol`.

Após editar o arquivo hosts, tente acessar o site pelo navegador. Se tudo deu certo, aparecerá a tela inicial de configuração do Wordpress.


# Conclusão

Se você chegou até aqui, parabéns! Você concluiu toda a criação do projeto... por enquanto. 

Como um dos principais pilares do DevOps é a **Integração Contínua**, seu código nunca deve parar de ser desenvolvido. Como próximos passos você pode implementar ferramentas de observabilidade, tornar a aplicação escalável no kubernetes, subir máquinas em outros *Cloud Providers* diferentes, automatizar ainda mais a instalação das dependências, corrigir bugs, etc.
