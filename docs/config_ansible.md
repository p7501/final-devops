# Configurar o Ansible

Após clonar o projeto, você deve realizar algumas configurações referentes ao Ansible.

## Instalar o Ansible

Para instalar o Ansible em uma distribuição linux baseada no Debian, por exemplo, o seguinte comando deve ser executado:

	sudo apt-get install ansible

Certifique-se de ter o python3 instalado nas **máquinas remotas**.

		sudo yum install python3

## Criar arquivo hosts
Primeiramente crie um arquivo chamado `hosts` dentro da pasta `src/ansible` e adicione os IPs, usuários e chaves SSH referentes às suas máquinas.

Exemplo:

~~~sh
[master-node]
10.0.0.30 ansible_user=oracle ansible_ssh_private_key_file="/home/usuario/.ssh/oracle-ssh"
~~~

* Caso tenha [provisionado instâncias EC2 pelo Terraform](./config_aws_ec2.md), o usuário padrão da instância se chama `ec2-user`.
