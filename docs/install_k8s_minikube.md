# Instalação do Kubernetes utilizando Minikube

Este documento apresenta duas formas de instalar o Kubernetes:
- Instalação manual: uma instalação onde você deve ter acesso direto ao terminal da máquina e digitar os comandos manualmente; 
- Instalação por esse [script](../src/scripts/install_k8s_minikube.sh).

Após a instalação do Minikube, [habilite o addon de ingress](#habilitando-o-addon-de-ingress).

## Instalação manual

Para instalar o minikube no Oracle Linux, primeiramente temos que baixar o pacote rpm do minikube:

		curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm

Após baixar o pacote, instale-o:

		sudo rpm -Uvh minikube-latest.x86_64.rpm

Inicie o minikube:

		minikube start

Verifique se está tudo ok:

		minikube status
		kubectl get pods -A
		kubectl version

Após verificar que está tudo ok, você pode [habilitar o ingress controller](#habilitando-o-addon-de-ingress).

## Instalação por script

Para realizar a instalação do minikube por [esse script](../src/scripts/install_k8s_minikube.sh), copie ele para sua máquina virtual:

		scp -r install_k8s_minikube.sh <USER>@<IP_MÁQUINA_VIRTUAL>:/home/oracle/install_k8s_minikube.sh

Dentro da máquina virtual execute o script, como usuário root:

		sudo chmod +x /home/oracle/install_k8s_minikube.sh
		sudo /home/oracle/install_k8s_minikube.sh

Verifique se está tudo ok:

		minikube status
		kubectl get pods -A
		kubectl version

Após verificar que está tudo ok, você pode [habilitar o ingress controller](#habilitando-o-addon-de-ingress).


## Habilitando o addon de ingress

Para habilitar o addon do ingress no minikube, execute o comando:

		minikube addons enable ingress

Para coferir se o ingress foi habilitado, execute o comando:

		minikube addons list | grep ingress


### Troubleshooting Docker driver

Caso você utilize o driver do docker, terá que fazer um redirecionamento de portas para poder acessar o minikube de outro dispositivo da sua rede LAN. Para fazer este redirecionamento, edite a tabela de IPs do Oracle Linux na máquina virtual:

		sudo iptables -A PREROUTING -t nat -i enp0s3 -p tcp --dport 80 -j DNAT --to 192.168.49.2:80
		sudo iptables -A FORWARD -p tcp -d 192.168.49.2 --dport 80 -j ACCEPT


## Próximo passo

Agora que o Kubernetes está instalado, você pode ir para a [instalação do Jenkins](docs/install_jenkins.md).


## Referencias

https://minikube.sigs.k8s.io/docs/start/

https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/
