# Instalação do Jenkins

Este documento apresenta uam forma de instalar o Jenkins pelo Minikube:
- Instalação com arquivos .yaml: uma instalação onde você deve ter acesso direto ao terminal da máquina; 

## Instalação com arquivos .yaml

Para instalar o Jenkins através do Minikube, primeiramente temos que estar com o Minikube ativado:

		minikube start

Após ativação já sera posivel utilizar os comandos kubectl para realizar o deploy. Pode se realizar os comandos dentro da pasta `src/jenkins`: 

		kubectl create -f jenkins-namespace.yaml
		kubectl create -f jenkins-deployment.yaml

Agora é só precisamos ativar o service:

		kubectl create -f jenkins-service.yaml

### Troubleshooting Docker driver

Ao utilizar o minikube com o driver do docker não é possível acessar o Jenkins na rede local, pois a interface de rede do minikube é diferente. Para acessar o Jenkins em algum dispositivo da rede local, é necessário redirecionar o ip do Minikube para o ip da sua máquina virtual. Utilizando o `iptables` do linux, é possível fazer isso:

		sudo iptables -A PREROUTING -t nat -i enp0s3 -p tcp --dport 30010 -j DNAT --to 192.168.49.2:30010
		sudo iptables -A FORWARD -p tcp -d 192.168.49.2 --dport 30010 -j ACCEPT

## Pós instalação

Após instalar o Jenkins, execute o comando abaixo para pegar a senha inicial do Jenkins:

		kubectl exec -it jenkins-deployment-5c9d9d8d9d-cj9qw -c jenkins -- /bin/bash
		sudo cat /var/jenkins_home/secrets/initialAdminPassword

Ou simplesmente:

		kubectl logs jenkins-deployment-5c9d9d8d9d-cj9qw

* Atenção: verifique o nome do pod do Jenkins, pois o mesmo pode mudar a cada execução.

Depois de pegar essas credenciais, basta acessar o Jenkins através do seguinte link:

		http://<IP_DA_VM>:<PORTA_DO_JENKINS>

<img src="assets/Captura de tela de 2022-02-28 14-55-25.png" alt="" width="700"/>

Coloque sua chave no *input* e prossiga com a instalação.

Na próxima tela você irá instalar os *plugins* do Jenkins.

Instale-os e prossiga.

<img src="assets/Captura de tela de 2022-02-28 14-56-42.png" alt="" width="700"/>

Pra finalizar, crie um usuário no Jenkins e acesse o painel de administração.

<img src="assets/Captura de tela de 2022-02-28 15-16-08.png" alt="" width="700"/>


Após esta configuração você pode criar seu pipeline no Jenkins.

## Referências

https://www.jenkins.io/doc/book/installing/kubernetes/
