# Configuração das Instâncias EC2

Nessa página estão descritos os passos para criar e configurar as instâncias EC2 da AWS.

## Pré-requisitos

### Par de chaves

Para acessarmos as máquinas, precisamos criar um par de chaves SSH.

		ssh-keygen -f aws-ssh -t rsa

O comando acima criará dois arquivos: uma chave públilca e uma chave privada. A chave privada **não** deve ser compartilhada com outras pessoas e deverá ser guardada em um local seguro. No linux é comum guardar as chaves SSH na masta `$HOME/.ssh`.

No console do AWS adicione a chave pública.

### Instalação do Terraform

Iremos utilizar o Terraform para poder provisionar as instâncias EC2.

Para instalar o Terraform, veja a documentação oficial: [Download and Install Terraform](https://www.terraform.io/downloads).


### AWS CLI

Você deverá estar com as credenciais de acesso a AWS na sua máquina local. O Terraform utiliza o AWS CLI para criar as instâncias EC2.

Para fazer isso, basta seguir a [documentação oficial da AWS](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-configure.html).


## Criação das instâncias EC2

As instâncias terão a distribuição RHEL 7.5. É o mais próximo do sistema Oracle Linux.

Primeiramente entre na pasta `src/terraform` e execute o comando:

		terraform init

Após iniciado, iremos ver o que será feito.

		terraform plan

Confira se está tudo ok e execute o comando:

		terraform apply

No final do comando você verá os IPs públicos das instâncias criadas.

Verifique se a conexão SSH funciona:

		ssh -i /local/da/chave/privada/aws-ssh ec2-user@<IP_PUBLICO>

Dentro da máquina mude o nome do usuário padrão para `oracle` e o nome dos hosts para `master` na t3a.small e `worker` nas t2.micro.

## Modificar nomes dos hosts

A partir daqui, iremos modificar os nomes dos hosts para `master` e `worker` nas t3a.small e `worker` nas t2.micro.

Entre na máquina `master` via SSH e execute o comando:

		sudo hostnamectl set-hostname master

Entre nas máquinas `worker` via SSH e execute o comando:

		sudo hostnamectl set-hostname worker

Se você tiver criado varios workers, por exemplo, você pode colocar uma numeração no nome do host.

		sudo hostnamectl set-hostname worker-01

* Por padrão, o usuário criado se chama `ec2-user`.



## Próximos passos

[Instalação do Docker](./install_docker.md)


## Referencias

https://www.terraform.io/downloads

https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-configure.html

https://www.alura.com.br/curso-online-terraform