#!/bin/bash

# Baixe o minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm

# Instale utilizando o rmpm
sudo rpm -ivh minikube-latest.x86_64.rpm

# Adicione o link do executável a uma pasta que está no PATH
sudo ln -s $(which minikube) /usr/local/bin/minikube

# Inicie o minikube
minikube start

# Verifique a instalação:
# minikube status
# kubectl -- get po -A
# kubectl version
