output "ip_master" {
  value = [aws_instance.master.public_ip]
}

output "ip_worker" {
  value = [aws_instance.worker.public_ip]
}
