#!/bin/bash

# Instala o git
# sudo yum install git -y

# Clona o repositório do nginx kubernetes ingress
git clone https://github.com/nginxinc/kubernetes-ingress/

# Entra na pasta
cd kubernetes-ingress/deployments
git checkout v2.1.1

# Configurações iniciais
## Criar namespace e service account
kubectl apply -f common/ns-and-sa.yaml

## Criar secret
kubectl apply -f common/default-server-secret.yaml

## Criar cluster role  binding para o service account
kubectl apply -f rbac/rbac.yaml

## Criar configmap para configurações customizadas
kubectl apply -f common/nginx-config.yaml

## Criar o recurso IngressClass
kubectl apply -f common/ingress-class.yaml

## Criar recursos customizados
kubectl apply -f common/crds/k8s.nginx.org_virtualservers.yaml
kubectl apply -f common/crds/k8s.nginx.org_virtualserverroutes.yaml
kubectl apply -f common/crds/k8s.nginx.org_transportservers.yaml
kubectl apply -f common/crds/k8s.nginx.org_policies.yaml

# Deploy do Ingress Controller
kubectl apply -f daemon-set/nginx-ingress.yaml